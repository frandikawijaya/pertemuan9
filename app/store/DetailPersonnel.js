Ext.define('Pertemuan9.store.DetailPersonnel', {
    extend: 'Ext.data.Store',

    alias: 'store.detailpersonnel',
    storeId:'detailpersonnel',
    // autoLoad: true,
    fields: [
        'name', 'email', 'phone'
    ],

      proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/MyApp_php/readDetailPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items' 
        }
    }
});